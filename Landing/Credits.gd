extends VBoxContainer

export(AudioStreamOGGVorbis) var hover_sound
export(AudioStreamOGGVorbis) var click_sound

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_Back_pressed():
	GlobalAudio.stream = click_sound
	GlobalAudio.play()
	get_tree().change_scene("res://Landing/LandingMenu.tscn")

func _on_Back_mouse_entered():
	GlobalAudio.stream = hover_sound
	GlobalAudio.play()
