extends Control

var selected

# Called when the node enters the scene tree for the first time.
func _ready():
	for cost in Build.build_cost:
		print(cost)
		var build_option = $Panel/menu.get_node(cost)
		if build_option:
			build_option.text += " (" + str(Build.build_cost[cost]) + ")"
		else:
			print("Warning: No such buildable: " + cost)

func select(build_platform):
	print(build_platform)
	print(build_platform.happiness)
	selected = build_platform
	$Panel/title.text = build_platform.display_names[build_platform.current]
	$Panel/menu/enabled.pressed = build_platform.enabled
	cost_allowance()

func cost_allowance():
	var world = get_tree().get_root().get_node("World")	
	for cost in Build.build_cost:
		var build_option = $Panel/menu.get_node(cost)
		if build_option:
			build_option.disabled = world.productivity < Build.build_cost[cost]
	
func _on_enabled_toggled(button_pressed):
	if selected:
		selected.enabled = button_pressed
