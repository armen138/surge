extends Spatial
var rng = RandomNumberGenerator.new()
var current = "vacant"
var powered = true
var enabled = true
var happiness = 0

var notification = {
	"unpowered": false,
	"unhappy": false
}

var x = 0
var y = 0

onready var world = get_tree().get_root().get_node("World")

var happiness_potential = {
	"neighborhood": 5,
	"solar_neighborhood": 5,
	"park": 10,
	"mall": 16,
	"offices": 12
}

var power_potential = {
	"powerplant": 50,
	"solar_farm": 15,
	"wind_farm": 25
}

var power_requirement = {
	"neighborhood": 8,
	"solar_neighborhood": 4,
	"mall": 32,
	"offices": 24
}

var display_names = {
	"neighborhood": "Neighborhood",
	"solar_neighborhood": "Solar Neighborhood",
	"park": "Park",
	"mall": "Mall",
	"offices": "Offices",
	"solar_farm": "Solar Farm",
	"wind_farm": "Wind Farm",
	"vacant": "Vacant Lot",
	"powerplant": "Powerplant"
}
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func build(building):
	current = building
	$AnimationPlayer.play(building)

func power_supply():
	var amount = 0
	if enabled and current in power_potential:
		amount = power_potential[current]
	return amount

func power_demand():
	var amount = 0
	if enabled and current in power_requirement:
		amount = power_requirement[current]
	return amount

func recalculate_happiness():
	if not enabled:
		powered = false
		happiness = 0
		return
	powered = world.power_demand < world.power_supply
	var nbrs = get_parent().neighbors(x, y)

	var happy = 0
	if "neighborhood" in current:
		for nbr in nbrs:
			if nbr == "offices":
				happy += 2
			if nbr == "mall":
				happy + 3
	if current == "offices":
		for nbr in nbrs:
			if "neighborhood" in nbr:
				happy += 2
	if current == "mall":
		for nbr in nbrs:
			if "neighborhood" in nbr:
				happy += 3
	if current == "park":
		happy = happiness_potential["park"]
	if current in happiness_potential:
		happiness = min(happy, happiness_potential[current])
	else:
		happiness = 0

func notify_unpowered(show):
	if show:
		if not notification["unpowered"]:
			$notifications/unpowered/notify.play('show')
			notification["unpowered"] = true
	else:
		if notification["unpowered"]:
			notification["unpowered"] = false
			$notifications/unpowered/notify.play('hide')

func notify_unhappy(show):
	if show:
		if not notification["unhappy"]:
			$notifications/unhappy/notify.play('show')
			notification["unhappy"] = true
	else:
		if notification["unhappy"]:
			notification["unhappy"] = false
			$notifications/unhappy/notify.play('hide')

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	recalculate_happiness()
	if !powered and current in power_requirement:
		notify_unpowered(true)
	elif current in power_requirement:
		notify_unpowered(false)
	if happiness == 0 and current in happiness_potential:
		notify_unhappy(true)
	elif current in happiness_potential:
		notify_unhappy(false)
