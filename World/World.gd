extends Spatial

var map_width = 10
var map_height = 10

var power_supply = 100
var power_demand = 100
var productivity = 0
var happiness = 100
var actions = 0

var map = []

var have_built = false
var select_mode = true
var selection

enum game_stages {
	BEGIN,
	UPGRADE,
	BUILD,
	WIN
}

var objectives = {
	game_stages.BEGIN: "1.begin.bb",
	game_stages.UPGRADE: "2.upgrade.bb",
	game_stages.BUILD: "3.build.bb",
	game_stages.WIN: "4.win.bb"
}
var current_stage = game_stages.BEGIN

onready var neighborhood = load("World/neighborhood.tscn")
onready var solar_neighborhood = load("World/solar_neighborhood.tscn")
onready var offices = load("World/offices.tscn")
onready var mall = load("World/mall.tscn")
onready var powerplant = load("World/powerplant.tscn")
onready var park = load("World/park.tscn")
onready var build_platform = load("World/BuildPlatform.tscn")

func create_town():
	var top_left = 4 
	var items = [
		"neighborhood",
		"neighborhood",
		"neighborhood",
		"neighborhood",
		"neighborhood",
		"neighborhood",
		"powerplant",
		"mall",
		"offices"
	]
	randomize()
	items.shuffle()
	for x in range(top_left, top_left + 3):
		for y in range(top_left, top_left + 3):
			map[x][y].build(items.pop_front())

func create_neighborhood(x, y):
	var nbh = build_platform.instance()
	nbh.translation.y = 0.5
	nbh.translation.x = x * 5
	nbh.translation.z = y * 5
	# game-grid coordinates
	nbh.x = x
	nbh.y = y
	add_child(nbh)
	return nbh

# Return all neighbors of tile (including tile itself)
# Do not return any disabled neighbors, since they don't count for happiness
func neighbors(x, y):
	var nbrs = []
	for h in range(x - 1, x + 2):
		for v in range(y - 1, y + 2):
			if h >= 0 && h < map.size() && v >=0 && v < map[h].size() && map[h][v].enabled:
				nbrs.append(map[h][v].current)
	return nbrs

func total_happiness():
	var happy = 0
	for x in range(0, map_width):
		for y in range(0, map_height):
			happy += map[x][y].happiness
	happiness = happy
	$CanvasLayer/current_stats/stats/happiness_stat/happiness.text = str(happy)
	

func total_power_supply():
	var supply = 0
	for x in range(0, map_width):
		for y in range(0, map_height):
			supply += map[x][y].power_supply()
	power_supply = supply
	$CanvasLayer/current_stats/stats/power_supply_stat/power_supply.text = str(supply)
	
func total_power_demand():
	var demand = 0
	for x in range(0, map_width):
		for y in range(0, map_height):
			demand += map[x][y].power_demand()
	power_demand = demand
	$CanvasLayer/current_stats/stats/power_demand_stat/power_demand.text = str(demand)

func all_buildings_enabled():
	for x in range(0, map_width):
		for y in range(0, map_height):
			if map[x][y].current != "vacant" and map[x][y].enabled == false:
				return false
	return true

func win_condition():
	for x in range(0, map_width):
		for y in range(0, map_height):
			if map[x][y].current != "vacant" and map[x][y].current != "powerplant" and map[x][y].enabled == false:
				return false
			if map[x][y].current == "powerplant" and  map[x][y].enabled == true:
				return false
	return true

func update_objective():
	var file = File.new()
	file.open("res://Resources/objectives/" + objectives[current_stage], File.READ)
	var content = file.get_as_text()
	$CanvasLayer/help_pop_in/help/help_tabs/Objective/objective_scroll/objective_text.bbcode_text = content
	file.close()
	$CanvasLayer/help_pop_in/help_open.text = "1"

func show_win_screen():
	var win_text = $CanvasLayer/win_screen/Panel/win_text.bbcode_text 
	win_text = win_text.replace("99", str(actions))
	$CanvasLayer/win_screen/Panel/win_text.bbcode_text = win_text
	$CanvasLayer/win_screen.show()

func progress():
	if current_stage == game_stages.BEGIN and power_supply > power_demand:
		current_stage = game_stages.UPGRADE
		return update_objective()
	if current_stage == game_stages.UPGRADE and power_supply > power_demand and all_buildings_enabled():
		current_stage = game_stages.BUILD
		return update_objective()
	if current_stage == game_stages.BUILD and power_supply > power_demand and all_buildings_enabled() and have_built:
		current_stage = game_stages.WIN
		return update_objective()
	if current_stage == game_stages.WIN and power_supply > power_demand and win_condition():
		return show_win_screen()

func _ready():
	map.resize(map_width)
	for x in range(0, map_width):
		map[x] = []
		map[x].resize(map_height)
		for y in range(0, map_height):
			map[x][y] = create_neighborhood(x, y)
	create_town()
	update_objective()

func _process(delta):
#	total_power_supply()
#	total_power_demand()
#	total_happiness()
#	$CanvasLayer/current_stats/stats/productivity_stat/productivity.text = str(productivity) #str(max(0, power_supply - power_demand))
	pass

func _physics_process(delta):
	if select_mode:
		var mouse_position = get_viewport().get_mouse_position()
		var from = $Camera.project_ray_origin(mouse_position)
		var to = from + $Camera.project_ray_normal(mouse_position) * 1000

		$RayCast.cast_to = to
		$RayCast.translation = from
		if $RayCast.is_colliding():
			var selection_translation = $RayCast.get_collider().get_parent().translation
			$selection.translation.x = selection_translation.x
			$selection.translation.z = selection_translation.z
			selection = $RayCast.get_collider().get_parent()

func _unhandled_input(event):
	if event.is_action("help") && event.is_pressed():
		$CanvasLayer/pop_in/pop_in_animator.play("help")

	if event.is_action("back") && event.is_pressed():
		set_select_mode()
	# if event.is_action("select") && event.is_pressed():
	if event is InputEventMouseButton:
		if select_mode:
			$CanvasLayer/pop_in.select(selection)
			$CanvasLayer/pop_in/pop_in_animator.play("pop_in")
			if $CanvasLayer/pop_in/pop_in_menu_animator.has_animation(selection.current):
				$CanvasLayer/pop_in/pop_in_menu_animator.play(selection.current)
			else:
				$CanvasLayer/pop_in/pop_in_menu_animator.play("default")
			select_mode = false

func set_select_mode():
#	$CanvasLayer/pop_in/pop_in_animator.play_backwards("pop_in")
	$CanvasLayer/pop_in/pop_in_animator.play("pop_out_menu")
	select_mode = true
	
func _on_oh_no_pressed():
	$CanvasLayer/Control/panel_animator.play("pop_out")


func _on_Timer_timeout():
	total_power_supply()
	total_power_demand()
	total_happiness()
	$CanvasLayer/current_stats/stats/productivity_stat/productivity.text = str(productivity) #str(max(0, power_supply - power_demand))
	productivity += max(0, power_supply - power_demand) / 10.0 * (1 + happiness / 100)
	$CanvasLayer/pop_in.cost_allowance()
	progress()

func deny():
	print("Not enough productivity to build")
	
func build(item):
	if productivity < Build.build_cost[item]:
		deny()
		return
	productivity -= Build.build_cost[item]
	selection.build(item)
	set_select_mode()
	have_built = true
	actions += 1

func _on_solar_upgrade_pressed():
	if productivity < Build.build_cost["solar_upgrade"]:
		deny()
		return
	productivity -= Build.build_cost["solar_upgrade"]
	selection.build("solar_neighborhood")
	set_select_mode()
	actions += 1

func _on_close_pressed():
	set_select_mode()

func _on_neighborhood_pressed():
	build("neighborhood")

func _on_solar_neighborhood_pressed():
	build("solar_neighborhood")

func _on_solar_farm_pressed():
	build("solar_farm")

func _on_wind_farm_pressed():
	build("wind_farm")

func _on_offices_pressed():
	build("offices")

func _on_mall_pressed():
	build("mall")

func _on_park_pressed():
	build("park")

func _on_close_help_pressed():
	$CanvasLayer/pop_in/pop_in_animator.play("help_out")

func _on_help_open_pressed():
	$CanvasLayer/help_pop_in/help_open.text = ""
	$CanvasLayer/pop_in/pop_in_animator.play("help")


func _on_win_button_pressed():
	get_tree().change_scene("res://Landing/LandingMenu.tscn")
