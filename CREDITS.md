# Credits

## Armen
 * Game design
 * Programming

## iarebatman
 * 3D modelling
 
## UnbrokenUnworn
  * Additional UI programming
  * UI Audio
 
Assets used under license:

## Assets by Kenney.nl used under cc0

 * CityKit
 * CommerceKit
 * NatureKit
 * SpaceKit
 * UrbanKit
 
## Icons

see Icons/icons_attribution.txt
