[b]Happiness[/b]

Happiness is an important measure in the game, as a low happiness will come with productivity penalties.
Disabling power to a building will reduce the building's happiness output to 0.

Building potential happiness yields are as follows:

[table=2]
[cell][b]Neighborhood[/b][/cell]
[cell]One for each resident, where 2 are generated from proximity to offices, and 3 from proximity to malls[/cell]

[cell][b]Offices[/b][/cell]
[cell]2 for each adjacent neighborhood[/cell]

[cell][b]Mall[/b][/cell]
[cell]3 for each adjacent neighborhood[/cell]

[cell][b]Park[/b][/cell]
[cell]Parks add 10 happiness that cannot be reduced, as parks do not require power to operate[/cell]
[/table]


