Now with a steady supply of [i]productivity[/i], you can start building new things! To further increase happiness, build a [b]park[/b] or new [b]neighborhoods[/b]. Note that [b]neighborhoods[b] get happiness bonuses from adjacent [b]offices[/b] and [b]malls[/b]!

You should also look at building sources of [i]renewable energy[/i] such as the [b]solar farm[/b] and the [b]wind farm[/b].
