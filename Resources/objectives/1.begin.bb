First, you should free up some power to generate [i]productivity[/i], This is done by disabling buildings. Your biggest power draws are the [b]offices[/b] and the [b]mall[/b].
Doing this will generate [b]some unhappiness[/b]. You'll have to make up for that later!
