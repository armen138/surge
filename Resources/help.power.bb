[b]Power[/b]

There are two measurements to keep track of - power supply, and demand. If demand is larger than supply, the 
entire power grid will shut down. If supply is larger than demand, the excess can be used for [i]productivity[/i].

[b]Ways to reduce power consumption[/b]

The simplest way to reduce power consumption is to disable buildings in town. Disabling a building affects [i]Happiness[/i]. 

Building power demands are as follows:

[table=2]
[cell][b]Neighborhood[/b][/cell]
[cell]8[/cell]

[cell][b]Solar Neighborhood[/b][/cell]
[cell]4[/cell]

[cell][b]Offices[/b][/cell]
[cell]24[/cell]

[cell][b]Mall[/b][/cell]
[cell]32[/cell]
[/table]
